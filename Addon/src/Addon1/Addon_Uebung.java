package Addon1; 


public class Addon_Uebung {
	private int id_nummer;
	private String bezeichnung;
	private double verkaufspreis;
	private int maxBestand;
	private int bestandSpieler;
	
	
	public void Addon(){
		
	}
	
	public void kauf(){
		bestandSpieler = bestandSpieler + 1;
	}
	
	public double getGesamtwert() {
		return verkaufspreis*bestandSpieler;
	}
	
	
	public int getId_nummer() {
		return id_nummer;
	}
	
	public void setId_nummer(int id_nummer) {
		this.id_nummer = id_nummer;
	}
	
	public String getBezeichnung() {
		return bezeichnung;
	}
	
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
	public double getVerkaufspreis() {
		return verkaufspreis;
	}
	
	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}
	
	public int getMaxBestand() {
		return maxBestand;
	}
	
	public void setMaxBestand(int maxBestand) {
		this.maxBestand = maxBestand;
	}
	
	public int getBestandSpieler() {
		return bestandSpieler;
	}
	
	public void setBestandSpieler(int bestandSpieler) {
		this.bestandSpieler = bestandSpieler;
	}

}
