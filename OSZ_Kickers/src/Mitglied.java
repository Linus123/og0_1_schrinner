
public class Mitglied {
	
	private String name;
	private int telefonnummer;
	private boolean jahresBeitragBezahlt;
	
	
	public Mitglied() {
		
	}
	
	
	
	public Mitglied(String name, int telefonnummer, boolean jahresBeitragBezahlt ) {
		
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getTelefonnummer() {
		return telefonnummer;
	}


	public void setTelefonnummer(int telefonnummer) {
		this.telefonnummer = telefonnummer;
	}


	public boolean getJahresBeitragBezahlt() {
		return jahresBeitragBezahlt;
	}


	public void setJahresBeitragBezahlt(boolean jahresBeitragBezahlt) {
		this.jahresBeitragBezahlt = jahresBeitragBezahlt;
	}
	
	
	
	

}
