
public class Mannschaftsleiter extends Spieler {
	
	private String nameMannschaft;
	private int persoehnlicherRabatt;
	
	public Mannschaftsleiter() {
		
	}
	
	public Mannschaftsleiter(String name, int telefonnummer, boolean jahresBeitragBezahlt, int trikotnummer, String spielposition, String nameMannschaft, int persoehnlicherRabatt) {
		super(name, telefonnummer, jahresBeitragBezahlt, trikotnummer, spielposition);
		
		this.nameMannschaft = nameMannschaft;
		this.persoehnlicherRabatt = persoehnlicherRabatt;
	}

	public String getNameMannschaft() {
		return nameMannschaft;
	}

	public void setNameMannschaft(String nameMannschaft) {
		this.nameMannschaft = nameMannschaft;
	}

	public int getPersoehnlicherRabatt() {
		return persoehnlicherRabatt;
	}

	public void setPersoehnlicherRabatt(int persoehnlicherRabatt) {
		this.persoehnlicherRabatt = persoehnlicherRabatt;
	}
	
	

}
