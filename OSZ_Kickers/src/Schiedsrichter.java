
public class Schiedsrichter extends Mitglied {
	
	private int anzahlGepfiffeneSpiele;
	
	public Schiedsrichter() {
		
	}
	
	public Schiedsrichter(String name, int telefonnummer, boolean jahresBeitragBezahlt, int anzahlGepfiffeneSpiele) {
		super(name, telefonnummer, jahresBeitragBezahlt);
		
		this.anzahlGepfiffeneSpiele = anzahlGepfiffeneSpiele;
	}

	public int getAnzahlGepfiffeneSpiele() {
		return anzahlGepfiffeneSpiele;
	}

	public void setAnzahlGepfiffeneSpiele(int anzahlGepfiffeneSpiele) {
		this.anzahlGepfiffeneSpiele = anzahlGepfiffeneSpiele;
	}
	
}
