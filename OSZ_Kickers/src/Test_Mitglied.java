
public class Test_Mitglied {

	public static void main(String[] args) {
		
		Mitglied m1 = new Mitglied();
		m1.setName("Linus");
		m1.setJahresBeitragBezahlt(true);
		m1.setTelefonnummer(7958395);
		
		Spieler s1 = new Spieler();
		s1.setName("Connor");
		s1.setJahresBeitragBezahlt(false);
		s1.setTelefonnummer(1234554);
		s1.setTrikotnummer(12);
		s1.setSpielposition("Mittelfeld");
		
		Trainer t1 = new Trainer();
		t1.setName("Robin");
		t1.setJahresBeitragBezahlt(true);
		t1.setTelefonnummer(121345);
		t1.setLizenzklasse('C');
		t1.setAufwandsentschaedigung(350);
		
		Schiedsrichter ref1 = new Schiedsrichter();
		ref1.setName("Piotr");
		ref1.setJahresBeitragBezahlt(false);
		ref1.setTelefonnummer(1235435);
		ref1.setAnzahlGepfiffeneSpiele(2);
		
		Mannschaftsleiter lead1 = new Mannschaftsleiter();
		lead1.setName("Can");
		lead1.setJahresBeitragBezahlt(true);
		lead1.setTelefonnummer(4383297);
		lead1.setTrikotnummer(11);
		lead1.setSpielposition("Sturm");
		lead1.setPersoehnlicherRabatt(35);
		lead1.setNameMannschaft("Die wilden Kerle");
		
		System.out.println("Spieler 1");
		System.out.println("Name: " + s1.getName());
		System.out.println("Jahresbeitrag bezahlt: " + s1.getJahresBeitragBezahlt());
		System.out.println("Telefonnummer: " + s1.getTelefonnummer());
		System.out.println("Trikotnummer: " + s1.getTrikotnummer());
		System.out.println("Spielposition: " + s1.getSpielposition());
		
		System.out.println("");
		
		System.out.println("Trainer 1");
		System.out.println("Name: " + t1.getName());
		System.out.println("Jahresbeitrag bezahlt: " + t1.getJahresBeitragBezahlt());
		System.out.println("Telefonnummer: " + t1.getTelefonnummer());
		System.out.println("Lizenzklasse: " + t1.getLizenzklasse());
		System.out.println("Aufwandsentsch�digung: " + t1.getAufwandsentschaedigung());
		
		System.out.println("");
		
		System.out.println("Schiedrichter 1");
		System.out.println("Name: " + ref1.getName());
		System.out.println("Jahresbeitrag bezahlt: " + ref1.getJahresBeitragBezahlt());
		System.out.println("Telefonnummer: " + ref1.getTelefonnummer());
		System.out.println("Anzahl an gepfiffenen Spielen: " + ref1.getAnzahlGepfiffeneSpiele());
		
		System.out.println("");
		
		System.out.println("Mannschaftsleiter 1");
		System.out.println("Name: " + lead1.getName());
		System.out.println("Jahresbeitrag bezahlt: " + lead1.getJahresBeitragBezahlt());
		System.out.println("Telefonnummer: " + lead1.getTelefonnummer());
		System.out.println("Trikotnummer: " + lead1.getTrikotnummer());
		System.out.println("Spielposition: " + lead1.getSpielposition());
		System.out.println("Pers�hnlicher Rabatt: " + lead1.getPersoehnlicherRabatt());
		System.out.println("Mannschafts Name: " + lead1.getNameMannschaft());
		
		

	}

}
